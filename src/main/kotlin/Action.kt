sealed class Action {
    object Right : Action()
    object Left : Action()
    object Move : Action()
    object Report : Action()
    data class Place(val x: Int, val y: Int, val f: Direction) : Action()
}