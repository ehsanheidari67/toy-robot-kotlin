data class TableSurface(val width: Int, val height: Int) {
    companion object {
        const val FIRST_POSITION = 0
    }
}