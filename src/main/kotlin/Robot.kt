data class Robot(val x: Int? = null, val y: Int? = null, val direction: Direction? = null)

fun act(robot: Robot, tableSurface: TableSurface, action: Action): Robot =
    when (action) {
        Action.Left -> {
            turn(robot, Action.Left)
        }
        Action.Right -> {
            turn(robot, Action.Right)
        }
        Action.Move -> {
            move(robot, tableSurface)
        }
        is Action.Place -> {
            place(robot = robot, placeAction = action, tableSurface = tableSurface)
        }
        Action.Report -> {
            report(robot, tableSurface)
            robot
        }
    }

fun turn(robot: Robot, action: Action): Robot =
    when (action) {
        Action.Left -> {
            when (robot.direction) {
                Direction.NORTH -> robot.copy(direction = Direction.WEST)
                Direction.SOUTH -> robot.copy(direction = Direction.EAST)
                Direction.WEST -> robot.copy(direction = Direction.SOUTH)
                Direction.EAST -> robot.copy(direction = Direction.NORTH)
                else -> robot
            }
        }
        Action.Right -> {
            when (robot.direction) {
                Direction.NORTH -> robot.copy(direction = Direction.EAST)
                Direction.SOUTH -> robot.copy(direction = Direction.WEST)
                Direction.WEST -> robot.copy(direction = Direction.NORTH)
                Direction.EAST -> robot.copy(direction = Direction.SOUTH)
                else -> robot
            }
        }
        else -> {
            throw Exception("Invalid action for the function turn")
        }
    }

fun move(robot: Robot, tableSurface: TableSurface): Robot =
    if (isRobotOnTable(robot, tableSurface)) {
        val nextRobot = moveRobot(robot)
        if (isRobotOnTable(nextRobot, tableSurface)) {
            nextRobot
        } else {
            robot
        }
    } else {
        robot
    }

fun place(robot: Robot, placeAction: Action.Place, tableSurface: TableSurface): Robot =
    if (isPositionValid(x = placeAction.x, y = placeAction.y, tableSurface = tableSurface)) {
        robot.copy(x = placeAction.x, y = placeAction.y, direction = placeAction.f)
    } else {
        robot
    }

fun isPositionValid(x: Int?, y: Int?, tableSurface: TableSurface) =
    x in TableSurface.FIRST_POSITION until tableSurface.width && y in TableSurface.FIRST_POSITION until tableSurface.height

fun isRobotOnTable(robot: Robot, tableSurface: TableSurface): Boolean = isPositionValid(robot.x, robot.y, tableSurface)

fun moveRobot(robot: Robot): Robot =
    if (robot.x != null && robot.y != null) {
        when (robot.direction) {
            Direction.NORTH -> {
                robot.copy(y = robot.y + 1)
            }
            Direction.SOUTH -> {
                robot.copy(y = robot.y - 1)
            }
            Direction.WEST -> {
                robot.copy(x = robot.x - 1)
            }
            Direction.EAST -> {
                robot.copy(x = robot.x + 1)
            }
            null -> {
                robot
            }
        }
    } else {
        robot
    }


fun report(robot: Robot, tableSurface: TableSurface) {
    if (isRobotOnTable(robot, tableSurface)) {
        println(robot.x.toString() + "," + robot.y.toString() + "," + robot.direction?.f)
    } else {
        println("Robot hasn't been placed yet")
    }
}

