fun main() {
    var robot = Robot()
    val tableSurface = TableSurface(width = 5, height = 5)
    while (true) {
        commandToAction(readLine()?.split(" ").orEmpty())?.let { action ->
            robot = act(robot, tableSurface, action)
        }
    }
}

fun commandToAction(commandParts: List<String>): Action? =
    when (commandParts.getOrNull(0)) {
        "MOVE" -> Action.Move
        "LEFT" -> Action.Left
        "RIGHT" -> Action.Right
        "REPORT" -> Action.Report
        "PLACE" -> {
            val placeParams = commandParts.getOrNull(1)?.split(",").orEmpty()
            val x = placeParams.getOrNull(0)?.toIntOrNull()
            val y = placeParams.getOrNull(1)?.toIntOrNull()
            val f = placeParams.getOrNull(2)?.toDirection()

            if (x != null && y != null && f != null) {
                Action.Place(x = x, y = y, f = f)
            } else {
                null
            }
        }
        else -> null
    }


fun String.toDirection() =
    when (this) {
        "NORTH" -> Direction.NORTH
        "SOUTH" -> Direction.SOUTH
        "EAST " -> Direction.EAST
        "WEST" -> Direction.WEST
        else -> null
    }